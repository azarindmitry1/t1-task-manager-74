package ru.t1.dazarin.tm.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.UserDtoRepository;
import ru.t1.dazarin.tm.model.dto.CustomUser;
import ru.t1.dazarin.tm.model.dto.RoleDto;
import ru.t1.dazarin.tm.model.dto.UserDto;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service("userDetailsService")
public class UserDetailServiceBean implements UserDetailsService {

    private final UserDtoRepository userDtoRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserDto user = userDtoRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        final List<RoleDto> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (final RoleDto role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

}
