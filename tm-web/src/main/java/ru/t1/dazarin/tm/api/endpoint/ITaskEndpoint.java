package ru.t1.dazarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping(value = "/findAll")
    List<TaskDto> findAll();

    @WebMethod
    @GetMapping(value = "/findById/{id}")
    TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping(value = "/create")
    TaskDto create();

    @WebMethod
    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id
    );

    @WebMethod
    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @WebMethod
    @PutMapping(value = "/update")
    TaskDto update(
            @WebParam(name = "taskDto", partName = "taskDto")
            @NotNull @RequestBody TaskDto taskDto
    );

}
