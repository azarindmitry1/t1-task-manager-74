package ru.t1.dazarin.tm.exception.user;

public final class ExistEmailException extends AbstractUserException {

    public ExistEmailException() {
        super("Error! Email is exist...");
    }

}
