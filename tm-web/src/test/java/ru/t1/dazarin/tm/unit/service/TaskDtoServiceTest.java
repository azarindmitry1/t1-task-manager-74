package ru.t1.dazarin.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.dazarin.tm.configuration.WebApplicationConfiguration;
import ru.t1.dazarin.tm.enumerated.Status;
import ru.t1.dazarin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dazarin.tm.exception.field.TaskIdEmptyException;
import ru.t1.dazarin.tm.exception.field.UserIdEmptyException;
import ru.t1.dazarin.tm.marker.UnitCategory;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;
import ru.t1.dazarin.tm.util.UserUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@Category(UnitCategory.class)
public class TaskDtoServiceTest {

    @NotNull
    private static String USER_ID;

    @NotNull
    @Autowired
    private TaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private TaskDto taskOne;

    @Before
    public void setUp() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        taskDtoService.deleteAll(USER_ID);
        taskOne = taskDtoService.create(USER_ID);
    }

    @After
    public void tearDown() {
        taskDtoService.deleteAll(USER_ID);
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findAll(null));
        @Nullable final List<TaskDto> projects = taskDtoService.findAll(USER_ID).stream().collect(Collectors.toList());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.findById(null, taskOne.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskDtoService.findById(USER_ID, null));
        @Nullable final TaskDto TaskDto = taskDtoService.findById(USER_ID, taskOne.getId());
        Assert.assertNotNull(TaskDto);
    }

    @Test
    public void create() {
        taskDtoService.deleteAll(USER_ID);
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.create(null));
        @NotNull final TaskDto TaskDto = taskDtoService.create(USER_ID);
        Assert.assertNotNull(TaskDto);
    }

    @Test
    public void save() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.save(null, taskOne));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskDtoService.save(USER_ID, null));
        @Nullable TaskDto TaskDto = taskDtoService.findById(USER_ID, taskOne.getId());
        Assert.assertNotNull(TaskDto);
        @NotNull final Status newStatus = Status.COMPLETED;
        Assert.assertNotEquals(newStatus, TaskDto.getStatus());
        TaskDto.setStatus(newStatus);
        taskDtoService.save(USER_ID, TaskDto);
        TaskDto = taskDtoService.findById(USER_ID, taskOne.getId());
        Assert.assertNotNull(TaskDto);
        Assert.assertEquals(TaskDto.getStatus(), newStatus);
    }

    @Test
    public void deleteById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteById(null, taskOne.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskDtoService.deleteById(USER_ID, null));
        @Nullable TaskDto TaskDto = taskDtoService.findById(USER_ID, taskOne.getId());
        Assert.assertNotNull(TaskDto);
        taskDtoService.deleteById(USER_ID, TaskDto.getId());
        TaskDto = taskDtoService.findById(USER_ID, TaskDto.getId());
        Assert.assertNull(TaskDto);
    }

    @Test
    public void delete() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteById(null, taskOne.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> taskDtoService.deleteById(USER_ID, null));
        @Nullable TaskDto TaskDto = taskDtoService.findById(USER_ID, taskOne.getId());
        Assert.assertNotNull(TaskDto);
        taskDtoService.deleteById(USER_ID, taskOne.getId());
        TaskDto = taskDtoService.findById(USER_ID, TaskDto.getId());
        Assert.assertNull(TaskDto);
    }

    @Test
    public void deleteAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskDtoService.deleteAll(null));
        Assert.assertEquals(1L, taskDtoService.findAll(USER_ID).size());
        taskDtoService.deleteAll(USER_ID);
        Assert.assertEquals(0L, taskDtoService.findAll(USER_ID).size());
    }

}
