package ru.t1.azarin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    public final static String NAME = "user-change-password";

    @NotNull
    public final static String DESCRIPTION = "Change user password.";

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setNewPassword(newPassword);
        userEndpoint.changePasswordResponse(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
