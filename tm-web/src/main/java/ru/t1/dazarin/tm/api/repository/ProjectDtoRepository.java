package ru.t1.dazarin.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import java.util.List;

@Repository
public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    ProjectDto findByUserIdAndId(String userId, String id);

    List<ProjectDto> findAllByUserId(String userId);

    void deleteByUserIdAndId(String userId, String id);

    void deleteByUserId(String userId);

}
