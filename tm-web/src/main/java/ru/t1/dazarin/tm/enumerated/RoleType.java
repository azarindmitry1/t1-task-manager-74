package ru.t1.dazarin.tm.enumerated;

public enum RoleType {

    USER,
    ADMINISTRATOR;

}
